# Live Updating TreeView


A live updating tree application - Full stack development

see usage here : https://live-updating-tree.herokuapp.com

# Development Stack
  
- RethinkDB
  
- Node.js + Express
  
- Angular JS
  
- Heroku & AWS for Deployment


### Installation


It requires [Node.js](https://nodejs.org/) to run.

Install the dependencies and start the server.


```sh
$ node app.js
```





