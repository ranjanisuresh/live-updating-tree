var app = angular.module('starterApp', ['ngMaterial','ngRoute','ngMessages'])
.run(function($http, $rootScope){
  var factoryData = [];
  $http.get("/treeView").success(function(response){      
      $rootScope.factoryData = response.data;           
});
});

app.factory('socket',function(){          //connecting to socket.io server
  var socket = io.connect('http://localhost:3000');
  return socket;
});

app.config(function($routeProvider){
      console.log('I am in the route');
      $routeProvider
          .when('/',{
                templateUrl: 'home.html'
          });
});

app.controller('asyncFacController', function($scope,$mdDialog,$http,socket,$rootScope) { 
      
    function getFactoryData() {
    $http.get("/treeView").success(function(response){
     
      
   });      

  socket.on('changeFeed',function(data) {   
    console.log('I am here at changeFeed');     
                                        //listening to new messages on the socket
    for(var facCounter = 0 ;facCounter < $rootScope.factoryData.length; facCounter++) {
      if($rootScope.factoryData[facCounter].id === data.id) {       
        $rootScope.factoryData[facCounter].children = data.children;
        $rootScope.factoryData[facCounter].upper_bound = data.upper_bound;
        $rootScope.factoryData[facCounter].lower_bound = data.lower_bound;
        $rootScope.factoryData[facCounter].number_of_children = data.number_of_children;
        $rootScope.$apply();

        $scope.factoryData[facCounter].children = data.children;
        $scope.factoryData[facCounter].upper_bound = data.upper_bound;
        $scope.factoryData[facCounter].lower_bound = data.lower_bound;
        $scope.factoryData[facCounter].number_of_children = data.number_of_children;
        $scope.$apply();
        //console.log($rootScope);
        break;
      }
    }
  }); 
}
});

app.controller('factoryController',function($scope,$mdDialog,$http,res,$rootScope,$mdToast) { 
  $scope.formData = {}; 
  $scope.factoryData = $rootScope.factoryData;
  function isEmpty(obj) {
      return Object.keys(obj).length === 0;
  }
  if (!isEmpty(res.formData)) {
    $scope.isUpdate = true;
    $scope.formData.id = res.formData.id;
    $scope.formData.name = res.formData.name;
    $scope.formData.lower_bound = res.formData.lower_Bound;
    $scope.formData.upper_bound = res.formData.upper_Bound;
    $scope.formData.number_of_children = res.formData.number_of_children;
  }
  
  $scope.createFactory = function(ev) {
    $mdDialog.hide();
    var factoryData = [];
    var childNodes = new Array($scope.formData.number_of_children);
        for(var i=0;i<$scope.formData.number_of_children;i++){
            childNodes[i]=Math.floor(Math.random() * ($scope.formData.upper_bound - $scope.formData.lower_bound + 1) + $scope.formData.lower_bound);
        }    
    var data = {
      "name" : $scope.formData.name,
      "number_of_children" : $scope.formData.number_of_children,
      "upper_bound" : $scope.formData.upper_bound,
      "lower_bound" : $scope.formData.lower_bound,
      "children": childNodes
    };       
    var message = {"title" : "", "message" : ""};
    $http.post('/treeView',data).success(function(response) {
      if(response.responseCode === 0) {
        message.title = "Success !";
        message.message = "Factory created";
        data["id"] = response.data.generated_keys[0];    
        console.log(response);
        $scope.factoryData.push(data);
        $rootScope.factoryData = $scope.factoryData;          
      } else {
        message.title = "Error !";
        message.message = "Error occured when creating Factory";
      }         
      
    });
    $mdToast.show(
      $mdToast.simple()
        .textContent('Factory Created Successfully!')        
        .hideDelay(2000)
    );
  }
  
  $scope.close_modal = function(){
    $mdDialog.hide();    
  }
  
 
  $scope.updateFactory = function(ev) { 

    $mdDialog.hide();
    console.log("Update");    
    var childNodes = new Array($scope.formData.number_of_children);
        for(var i=0;i<$scope.formData.number_of_children;i++){
            childNodes[i]=Math.floor(Math.random() * ($scope.formData.upper_bound - $scope.formData.lower_bound + 1) + $scope.formData.lower_bound);
        }
    var data = {
      "id" : $scope.formData.id,
      "name" : $scope.formData.name,
      "number_of_children" : $scope.formData.number_of_children,
      "upper_bound" : $scope.formData.upper_bound,
      "lower_bound" : $scope.formData.lower_bound,
      "children": childNodes
    };
    
    $http.put('/treeView',data).success(function(response) {
      console.log(response);
      console.log($scope.factoryData);
      console.log($rootScope.factoryData);
      var message = {"title" : "", "message" : ""};
      if(response.responseCode === 0) {
        message.title = "Success !";
        message.message = "Factory updated";  
        for(var facCounter = 0 ;facCounter < $scope.factoryData.length; facCounter++) {
        if($scope.factoryData[facCounter].id === data.id) {
        
        $scope.factoryData.splice(facCounter, 1);
        $scope.factoryData.push(data);
        break;
      }
    }
        
        $rootScope.factoryData = $scope.factoryData; 
      } else {
        message.title = "Error !";
        message.message = "Error occured when updating Factory";
      }
    });
    $mdToast.show(
      $mdToast.simple()
        .textContent('Factory Updated Successfully!')        
        .hideDelay(20000)
    );
  }


});

app.controller( 'AppCtrl', function($scope, $mdDialog,$http,$rootScope, $mdToast) {  

$mdToast.show(
      $mdToast.simple()
        .textContent('Click on the Factory names to view child Nodes')        
        .hideDelay(5000)
    );  
  $scope.factoryModal = function(ev, data) {     
     var res = {};
     res.formData = data;
     $mdDialog.show({
     controller: 'factoryController',
     templateUrl: 'cuFac.tmpl.html',
     parent: angular.element(document.body),
     targetEvent: ev,
     clickOutsideToClose:true,
     fullscreen: $scope.customFullscreen,
     resolve: {
         res: function () {
           return res;
         }
       }

    })
    .then(function() {
      
    }, function() {

    });
  }

$scope.deleteFactory = function(ev, facInfo){   
    $scope.factoryData = $rootScope.factoryData;
    console.log("Inside deleteFactory", facInfo);  
    $http({
    method: 'DELETE',
    url: '/treeView',
    data: {"id" : facInfo.id},
    headers: {'Content-Type': 'application/json;charset=utf-8'}
    }).then( function(response){
     if(response.status === 200) {
        for(var facCounter = 0 ;facCounter < $scope.factoryData.length; facCounter++) {

        if($scope.factoryData[facCounter].id === facInfo.id) {
        $scope.factoryData.splice(facCounter, 1);
        break;
      }
    }

    $rootScope.factoryData = $scope.factoryData;

     }      
   });
    $mdToast.show(
      $mdToast.simple()
        .textContent('Factory Deleted!')        
        .hideDelay(1000)
    );

}

});


